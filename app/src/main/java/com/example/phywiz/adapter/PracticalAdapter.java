package com.example.phywiz.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.phywiz.R;
import com.example.phywiz.ScreenSlidePagerActivity;
import com.example.phywiz.model.ModelClassofInstrument;
import com.example.phywiz.model.ModelClassofPractical;

import java.util.List;

public class PracticalAdapter extends RecyclerView.Adapter<PracticalAdapter.HolderForExplore> {

    private Context mcontextt;
    private List<ModelClassofPractical> modelClassofPracticalList;

    public PracticalAdapter(Context mcontext, List<ModelClassofPractical> modelClassofPracticalListt) {
        this.mcontextt = mcontext;
        this.modelClassofPracticalList = modelClassofPracticalListt;
    }


    @NonNull
    @Override
    public PracticalAdapter.HolderForExplore onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_theorytopics, parent, false);
        return new PracticalAdapter.HolderForExplore(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PracticalAdapter.HolderForExplore holder, final int position) {

        holder.name.setText(modelClassofPracticalList.get(position).getName());
        holder.description.setText(modelClassofPracticalList.get(position).getDescription());

        holder.card_rv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mcontextt, ""
                        + modelClassofPracticalList.get(position).getName()
                        + " " + modelClassofPracticalList.get(position).getDescription(), Toast.LENGTH_SHORT).show();

                Intent i = new Intent(mcontextt, ScreenSlidePagerActivity.class);
                i.putExtra("key1", "" + modelClassofPracticalList.get(position).getName());
                i.putExtra("key2", "" + modelClassofPracticalList.get(position).getDescription());
                mcontextt.startActivity(i);
            }
        });

    }

//    @Override
//    public void onBindViewHolder(@NonNull InstrumentAdapter.HolderForExplore holder, final int position) {
//        holder.name.setText(modelClassofInstrumentsList.get(position).getName());
//        holder.description.setText(modelClassofInstrumentsList.get(position).getDescription());
//
//        holder.card_rv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(mcontextt, ""
//                        +modelClassofInstrumentsList.get(position).getName()
//                        +" "+modelClassofInstrumentsList.get(position).getDescription(), Toast.LENGTH_SHORT).show();
//
//                Intent i = new Intent(mcontextt, ScreenSlidePagerActivity.class);
//                i.putExtra("key1",""+modelClassofInstrumentsList.get(position).getName());
//                i.putExtra("key2",""+modelClassofInstrumentsList.get(position).getDescription());
//                mcontextt.startActivity(i);
//            }
//        });
//
//    }

//    @Override
//    public void onBindViewHolder(@NonNull final TheoryAdapter.HolderForExplore holder, final int position) {
//        holder.name.setText(modelClassofInstrumentsList.get(position).getName());
//        holder.description.setText(modelClassofInstrumentsList.get(position).getDescription());
//
//        holder.card_rv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(mcontextt, ""
//                        +modelClassofInstrumentsList.get(position).getName()
//                        +" "+modelClassofInstrumentsList.get(position).getDescription(), Toast.LENGTH_SHORT).show();
//
//                Intent i = new Intent(mcontextt, ScreenSlidePagerActivity.class);
//                i.putExtra("key1",""+modelClassofInstrumentsList.get(position).getName());
//                i.putExtra("key2",""+modelClassofInstrumentsList.get(position).getDescription());
//                mcontextt.startActivity(i);
//            }
//        });
//
//


    @Override
    public int getItemCount() {
        Log.e("theorysize", modelClassofPracticalList.size() + "");
        return modelClassofPracticalList.size();
    }

    public static class HolderForExplore extends RecyclerView.ViewHolder {
        CardView card_rv;
        TextView name, description;
        RelativeLayout relativeLayout;

        public HolderForExplore(View itemView) {
            super(itemView);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.rellayout);

            card_rv = (CardView) itemView.findViewById(R.id.card_RV);
            name = (TextView) itemView.findViewById(R.id.textView_name);
            description = (TextView) itemView.findViewById(R.id.textView_description);

        }
    }
}



