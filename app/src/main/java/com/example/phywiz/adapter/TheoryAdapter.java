package com.example.phywiz.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.phywiz.R;
import com.example.phywiz.ScreenSlidePagerActivity;
import com.example.phywiz.model.ModelClassofTheory;

import java.util.List;

public class TheoryAdapter extends RecyclerView.Adapter<TheoryAdapter.HolderForExplore> {

    private Context mcontextt;
    private List<ModelClassofTheory> modelClassofTheoryList;

    public TheoryAdapter(Context mcontext, List<ModelClassofTheory> modelClassofTheoryListt) {
        this.mcontextt = mcontext;
        this.modelClassofTheoryList = modelClassofTheoryListt;
    }


    @NonNull
    @Override
    public TheoryAdapter.HolderForExplore onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_theorytopics, parent, false);
        return new TheoryAdapter.HolderForExplore(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HolderForExplore holder, final int position) {
        holder.name.setText(modelClassofTheoryList.get(position).getName());
        holder.description.setText(modelClassofTheoryList.get(position).getDescription());

        holder.card_rv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mcontextt, ""
                        +modelClassofTheoryList.get(position).getName()
                        +" "+modelClassofTheoryList.get(position).getDescription(), Toast.LENGTH_SHORT).show();

                Intent i = new Intent(mcontextt, ScreenSlidePagerActivity.class);
                i.putExtra("key1",""+modelClassofTheoryList.get(position).getName());
                i.putExtra("key2",""+modelClassofTheoryList.get(position).getDescription());
                mcontextt.startActivity(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        Log.e("theorysize", modelClassofTheoryList.size() + "");
        return modelClassofTheoryList.size();
    }

    public static class HolderForExplore extends RecyclerView.ViewHolder {
        CardView card_rv;
        TextView name , description;
        RelativeLayout relativeLayout;

        public HolderForExplore(View itemView) {
            super(itemView);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.rellayout);

            card_rv = (CardView)itemView.findViewById(R.id.card_RV);
            name = (TextView) itemView.findViewById(R.id.textView_name);
            description = (TextView) itemView.findViewById(R.id.textView_description);

        }
    }

}
