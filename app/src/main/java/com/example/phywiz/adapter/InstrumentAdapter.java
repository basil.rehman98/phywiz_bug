package com.example.phywiz.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.phywiz.R;
import com.example.phywiz.ScreenSlidePagerActivity;
import com.example.phywiz.model.ModelClassofInstrument;

import java.util.List;

public class InstrumentAdapter extends RecyclerView.Adapter<InstrumentAdapter.HolderForExplore> {

    private Context mcontextt;
    private List<ModelClassofInstrument> modelClassofInstrumentsList;

    public InstrumentAdapter(Context mcontext, List<ModelClassofInstrument> modelClassofInstrumentsListt) {
        this.mcontextt = mcontext;
        this.modelClassofInstrumentsList = modelClassofInstrumentsListt;
    }


    @NonNull
    @Override
    public InstrumentAdapter.HolderForExplore onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_theorytopics, parent, false);
        return new InstrumentAdapter.HolderForExplore(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderForExplore holder, final int position) {
        holder.name.setText(modelClassofInstrumentsList.get(position).getName());
        holder.description.setText(modelClassofInstrumentsList.get(position).getDescription());

        holder.card_rv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mcontextt, ""
                        +modelClassofInstrumentsList.get(position).getName()
                        +" "+modelClassofInstrumentsList.get(position).getDescription(), Toast.LENGTH_SHORT).show();

                Intent i = new Intent(mcontextt, ScreenSlidePagerActivity.class);
                i.putExtra("key1",""+modelClassofInstrumentsList.get(position).getName());
                i.putExtra("key2",""+modelClassofInstrumentsList.get(position).getDescription());
                mcontextt.startActivity(i);
            }
        });

    }

//    @Override
//    public void onBindViewHolder(@NonNull final TheoryAdapter.HolderForExplore holder, final int position) {
//        holder.name.setText(modelClassofInstrumentsList.get(position).getName());
//        holder.description.setText(modelClassofInstrumentsList.get(position).getDescription());
//
//        holder.card_rv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(mcontextt, ""
//                        +modelClassofInstrumentsList.get(position).getName()
//                        +" "+modelClassofInstrumentsList.get(position).getDescription(), Toast.LENGTH_SHORT).show();
//
//                Intent i = new Intent(mcontextt, ScreenSlidePagerActivity.class);
//                i.putExtra("key1",""+modelClassofInstrumentsList.get(position).getName());
//                i.putExtra("key2",""+modelClassofInstrumentsList.get(position).getDescription());
//                mcontextt.startActivity(i);
//            }
//        });
//
//
//    }

    @Override
    public int getItemCount() {
        Log.e("theorysize", modelClassofInstrumentsList.size() + "");
        return modelClassofInstrumentsList.size();
    }

    public static class HolderForExplore extends RecyclerView.ViewHolder {
        CardView card_rv;
        TextView name , description;
        RelativeLayout relativeLayout;

        public HolderForExplore(View itemView) {
            super(itemView);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.rellayout);

            card_rv = (CardView)itemView.findViewById(R.id.card_RV);
            name = (TextView) itemView.findViewById(R.id.textView_name);
            description = (TextView) itemView.findViewById(R.id.textView_description);

        }
    }

}

