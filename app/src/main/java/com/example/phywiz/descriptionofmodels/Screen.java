package com.example.phywiz.descriptionofmodels;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.phywiz.R;

public class Screen extends Fragment {

    public static Fragment newInstance(String key1) {
        Screen s = new Screen();
        Bundle n = new Bundle();
        n.getString("key1",key1);
        s.setArguments(n);
        return s;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_screen,container,false);
//        TextView tv = (TextView) v.findViewById(R.id.tv_swipe_sc);
//        tv.setText(getArguments().getString("key1"));
//        tv.setText(("key1"));
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_screen, container, false);
        return v;
    }
}