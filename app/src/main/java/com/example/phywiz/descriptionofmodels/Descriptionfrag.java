package com.example.phywiz.descriptionofmodels;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.phywiz.R;

public class Descriptionfrag extends Fragment {
    public static Fragment newInstance(String key2) {
        Descriptionfrag D = new Descriptionfrag();
        Bundle n = new Bundle();
        n.putString("key2",key2);
        D.setArguments(n);
        return D;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_descriptionfrag,container,false);
//        TextView tv = (TextView) v.findViewById(R.id.text_fragS);
//        tv.setText(getArguments().getString("key2"));
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_descriptionfrag, container, false);
        return v;
    }
}