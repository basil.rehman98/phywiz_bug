package com.example.phywiz.descriptionofmodels;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.phywiz.R;

public class Descriptionofmodelspg2 extends Fragment {
    public static Fragment newInstance(String key3) {
        Descriptionfrag D = new Descriptionfrag();
        Bundle n = new Bundle();
        n.putString("key3", key3);
        D.setArguments(n);
        return D;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_descriptionofmodelspg2,container,false);
//        TextView tv = (TextView) v.findViewById(R.id.des_mod_2);
//        tv.setText(getArguments().getString("key3"));
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_descriptionofmodelspg2, container, false);
        return v;
    }
}
