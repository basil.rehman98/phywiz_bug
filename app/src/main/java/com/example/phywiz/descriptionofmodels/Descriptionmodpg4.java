package com.example.phywiz.descriptionofmodels;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.phywiz.R;

public class Descriptionmodpg4 extends Fragment {
    public static Fragment newInstance(String key5) {
        Descriptionfrag D = new Descriptionfrag();
        Bundle n = new Bundle();
        n.putString("key5", key5);
        D.setArguments(n);
        return D;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_descriptionmodpg4,container,false);
//        TextView tv = (TextView) v.findViewById(R.id.des_mod_4);
//        tv.setText(getArguments().getString("key5"));
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_descriptionmodpg4, container, false);
        return v;
    }
}