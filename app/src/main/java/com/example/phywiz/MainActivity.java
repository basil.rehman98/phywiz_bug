package com.example.phywiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
//    private Button button;
    private int progbar = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(MainActivity.this ,ScreenSlidePagerActivity.class));
                finish();
            }
        },progbar);

//        button = (Button)findViewById(R.id.button);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openMenu();
//            }
//        });
    }
//
//    private void openMenu() {
//        Intent intent = new Intent(this, Menu.class);
//        startActivity(intent);
//    }
    }
