package com.example.phywiz;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.phywiz.descriptionofmodels.Desciptionmodpg3;
import com.example.phywiz.descriptionofmodels.Descriptionfrag;
import com.example.phywiz.descriptionofmodels.Descriptionmodpg4;
import com.example.phywiz.descriptionofmodels.Descriptionofmodelspg2;
import com.example.phywiz.descriptionofmodels.Screen;
import com.test2.test3.UnityPlayerActivity;

public class ScreenSlidePagerActivity extends FragmentActivity {
    static final int NUM_PAGES = 5 ;

    private ViewPager mPager;
    String key1="key1",key2="key2",key3="key3",key4="key4",key5="key5";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slide);
//        Intent i = new Intent(this, UnityPlayerActivity.class);
//        startActivity(i);
//        loadFragment();
//        Intent intent = getIntent();
//        if (intent != null) {
//            key1 = intent.getStringExtra("key1");
//            Log.e("checkdata","checking");
//            key2 = intent.getStringExtra("key2");
//        }
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new ScreenSlidePagerAdapter(getSupportFragmentManager()));
        mPager.setPageTransformer(true,new SwipeDetector());
//        mPager.setPageTransformer(true, new SwipeDetector());

    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {


        public ScreenSlidePagerAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            Toast.makeText(ScreenSlidePagerActivity.this, ""+position, Toast.LENGTH_SHORT).show();
            switch (position) {
                case 0:
                    return Screen.newInstance(key1);
                case 1:
                    return Descriptionfrag.newInstance(key2);
                case 2:
                    return Descriptionofmodelspg2.newInstance(key3);
                case 3:
                    return Desciptionmodpg3.newInstance(key4);
                case 4:
                    return Descriptionmodpg4.newInstance(key5);
                default: return Screen.newInstance(key1);
            }

        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}